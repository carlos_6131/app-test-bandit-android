package com.example.icarlos.test_app_android.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.icarlos.test_app_android.data.AbstractLocation.LocationEntry;

/**
 * Created by servitrackgps on 16/1/17.
 */

public class LocationDBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Locations.db";

    public LocationDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL Commands
        db.execSQL("CREATE TABLE " + LocationEntry.TABLE_NAME + " ("
                + LocationEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + LocationEntry.LAT + " TEXT NOT NULL,"
                + LocationEntry.LNG + " TEXT NOT NULL, "
                + LocationEntry.REPORTED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Nothing to do
    }
}
