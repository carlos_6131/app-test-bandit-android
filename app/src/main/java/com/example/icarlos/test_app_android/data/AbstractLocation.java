package com.example.icarlos.test_app_android.data;


import android.provider.BaseColumns;

/**
 * Created by servitrackgps on 16/1/17.
 */

public class AbstractLocation {
    public static abstract class LocationEntry implements BaseColumns {
        public static final String TABLE_NAME = "Locations";

        public static final String ID = "id";
        public static final String LAT = "lat";
        public static final String LNG = "lng";
        public static final String REPORTED_AT = "reported_at";
    }
}
