package com.example.icarlos.test_app_android;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.icarlos.test_app_android.data.AbstractLocation.LocationEntry;
import com.example.icarlos.test_app_android.data.LocationDBHelper;

public class HistoryLocationController extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_location_controller);

        ListView mListView = (ListView) findViewById(R.id.lv_my_locations_history);

        SimpleCursorAdapter mLocationsAdapter = new SimpleCursorAdapter(
                this,
                android.R.layout.two_line_list_item,
                getAllLocations(),
                new String[]{LocationEntry.LAT, LocationEntry.LNG},
                new int[]{android.R.id.text1, android.R.id.text2},
                SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        );

        mListView.setAdapter(mLocationsAdapter);
    }

    private Cursor getAllLocations(){
        LocationDBHelper locationDBHelper = new LocationDBHelper(this, LocationEntry.TABLE_NAME, null, 1);
        return locationDBHelper.getReadableDatabase().query(
                LocationEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }
}
