package com.example.icarlos.test_app_android;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.icarlos.test_app_android.data.AbstractLocation.LocationEntry;
import com.example.icarlos.test_app_android.data.LocationDBHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class LocationController extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        OnMapReadyCallback,
        LocationListener{

    private static final int REQUEST_CONFIG_LOCATION = 101;
    private static final int REQUEST_PERMISSION_LOCATION = 201;
    private static final int GENERAL_INTERVAL = 10000;
    private static final int FAST_INTERVAL = 5000;

    private static String LOGTAG = "ANDROID_LOCATION";


    private GoogleApiClient googleApiClient;
    private GoogleMap mMap;
    private Marker mMarker;

    private Location lastLocation;
    private LocationRequest locationRequest;

//    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_controller);

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
            googleApiClient.connect();
        }

        if (googleApiClient == null) {
            toggleLocationUpdates(false);
        } else {
            toggleLocationUpdates(true);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.show_details);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LocationController.this, HistoryLocationController.class);
                startActivity(intent);
                Toast.makeText(LocationController.this, "U CLICKED ME!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    @SuppressWarnings("MissingPermission")
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_LOCATION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                mMap.setMyLocationEnabled(true);
                if (mMarker != null && lastLocation != null) {
                    mMarker.remove();
                }
                // TODO: Obtener LatLng y guardar en SQL
                LatLng mapCenter = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
                setMarkerOnMap(mapCenter);
            } else {
                Log.e(LOGTAG, "Permiso denegado");
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSION_LOCATION);
        } else {
            if (isLocationServiceEnabled()) {
                lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                if (mMarker != null && lastLocation != null) {
                    mMarker.remove();
                }
                // TODO: Obtener LatLng y guardar en SQL
                LatLng mapCenter = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
                setMarkerOnMap(mapCenter);
            } else {
                enableLocationServiceSettings();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "No se logro establecer con Google Services. Verifica tu conexion.", Toast.LENGTH_SHORT).show();
        Log.e(LOGTAG, "Error grave al conectar con Google Play Services.");
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;
        if (mMarker != null && lastLocation != null) {
            mMarker.remove();
        }
        LatLng mapCenter = new LatLng(location.getLatitude(), location.getLongitude());
        setMarkerOnMap(mapCenter);
    }


    private void toggleLocationUpdates(boolean enable) {
        if (enable) {
            enableLocationUpdates();
        } else {
            disableLocationUpdates();
        }
    }

    public void enableLocationUpdates() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(GENERAL_INTERVAL);
        locationRequest.setFastestInterval(FAST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        final LocationSettingsRequest locationSettingsRequest =
                new LocationSettingsRequest.Builder()
                        .addLocationRequest(locationRequest)
                        .build();

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        googleApiClient,
                        locationSettingsRequest
                );

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startLocationUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(LocationController.this, REQUEST_PERMISSION_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            toggleLocationUpdates(false);
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        toggleLocationUpdates(false);
                        break;
                }
            }
        });
    }

    private void disableLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    googleApiClient, locationRequest, this);
        }
    }

    private boolean isLocationServiceEnabled() {
        LocationManager locationManager = null;
        boolean gps_enabled = false, network_enabled = false;

        if (locationManager == null)
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            //do nothing...
        }
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            //do nothing...
        }

        return gps_enabled || network_enabled;

    }

    private void enableLocationServiceSettings() {
        Intent intentLocationService = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        this.startActivity(intentLocationService);
    }

    private void setMarkerOnMap(LatLng latLng) {
        saveOnDB(String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));
        MarkerOptions myMarkerOptions = new MarkerOptions();
        myMarkerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        myMarkerOptions.position(latLng);
        myMarkerOptions.title("Mi posicion actual");

        mMarker = mMap.addMarker(myMarkerOptions);
        mMarker.showInfoWindow();
    }

    private void saveOnDB(String lat, String lng){
        LocationDBHelper locationDBHelper = new LocationDBHelper(this, LocationEntry.TABLE_NAME, null, 1);
        SQLiteDatabase db = locationDBHelper.getWritableDatabase();
        if (db != null){
            db.execSQL("INSERT INTO " + LocationEntry.TABLE_NAME
                    + "(" + LocationEntry.LAT + ", " + LocationEntry.LNG + ")"
                    + "VALUES ('"+ lat +"','"+ lng +"')");
            db.close();
        }
    }
}